package anon.trollegle;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

public class Commands {

    private static abstract class Command {

        public String usage;
        private final String helpstring;

        public Command(String name, String helpstring) {
            this.usage = name;
            this.helpstring = helpstring;
        }

        public String getHelpString(Multi m, UserConnection target) {
            return this.helpstring;
        }

        public abstract void process(Multi m, String[] args, UserConnection target);
    }

    private static final HashMap<String, Command> adminCommands = new HashMap<>();

    private static String argsToString(int offset, String[] args) {
        StringBuilder sb = new StringBuilder();

        for (int i = offset; i < args.length; i++) {
            sb.append(args[i]).append(' ');
        }

        sb.setLength(Math.max(sb.length() - 1, 0)); //remove last space

        return sb.toString();
    }

    static {
        //TODO: add arglen checking
        //TODO: refuck list

        addAdminCommand("hello", new Command("hello", "Says hello!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                if (Math.random() > 0.1) {
                    target.schedTell("Hello, friend!");
                } else {
                    target.schedTell("F*ck off, b*tch >:C");
                }
            }
        });

        addAdminCommand("help", new Command("help", "Shows help, not unlike this command!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                StringBuilder sb = new StringBuilder("Showing admin command help:");

                for (Command c : adminCommands.values()) {
                    if (c.helpstring != null) {   // use null helpstring to hide a command
                        sb.append("\n———\u2003").append(c.usage).append("\u2003———\n\u2003").append(c.getHelpString(m, target));
                    }
                }

                sb.append("\n\n——\u2003Spacecat's Preliminary Edition");

                target.schedTell(sb.toString());
            }
        });

        addAdminCommand("broad", new Command("broad MESSAGE", "Yell to the people in the room!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.hear(target, argsToString(0, args));
            }
        });

        addAdminCommand("kick", new Command("kick PERSON", "Kick that cheeky motherfucker! >:o") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.kick(args[0]);
            }
        });

        addAdminCommand("mute", new Command("mute PERSON", "Make them shut up for reals!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.mute(args[0]);
            }
        });

        addAdminCommand("unmute", new Command("unmute PERSON", "Decide that person was actually sorta fun and give them one more try.") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.unmute(args[0]);
            }
        });

        addAdminCommand("invite", new Command("invite", "<dynamic>") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.add();
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Invite a user, " + m.qfreq + " from question mode";
            }
        });

        addAdminCommand("qfreq", new Command("qfreq DECIMAL", "Set the question mode fraction to FREQ") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                double f = Double.valueOf(args[0]);
                if (f >= 0 && f <= 1) {
                    m.qfreq = f;
                } else {
                    target.schedTell("Must be between 0 and 1");
                }
            }
        });

        addAdminCommand("invitep", new Command(null, "Invite a returning user (\"pulse\") from chat mode") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.addPulse();
            }
        });

        addAdminCommand("inviteq", new Command(null, "Invite a user from question mode") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.add(true);
            }
        });

        addAdminCommand("invitec", new Command(null, "Invite a user from chat mode") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.add(null);
            }
        });

        addAdminCommand("list", new Command(null, "List all users, including untriaged") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                //TODO: try to clean up list v:
                StringBuilder list = new StringBuilder();
                for (UserConnection u : m.users) {
                    synchronized (m.welcomed) {
                        list.append("\n * " + u.getNick() + (u.isPulse() ? " (pulse)"
                                : u.isAccepted() ? m.welcomed.contains(u) ? "" : u.isMuted() ? " (muted)" : " (dead/zombie)" : " (untriaged)"));
                    }
                }
                if (list.length() > 0) {
                    target.schedTell(list.substring(1));
                }
            }
        });

        addAdminCommand("freq", new Command("freq FREQ", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                int f = Integer.valueOf(args[0]);
                if (f >= 250) {
                    m.joinFreq = f;
                } else {
                    target.schedTell("Must be 250 or greater (milliseconds, not seconds)");
                }
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Set the spam frequency to FREQ (currently " + m.joinFreq + ") milliseconds";
            }
        });

        addAdminCommand("pfreq", new Command("pfreq FREQ", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                int f = Integer.valueOf(args[0]);
                m.pulseFreq = f;
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Set the pulse frequency to FREQ (currently \" + m.pulseFreq + \") milliseconds";
            }
        });

        addAdminCommand("verbose", new Command("verbose|terse", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                UserConnection.verbose = true;
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Verbose output, including system messages sent to users (currently "
                        + (UserConnection.verbose ? "verbose" : "terse") + ")";
            }
        });

        addAdminCommand("terse", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                UserConnection.verbose = true;
            }
        });

        addAdminCommand("murder", new Command("murder|revive", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.murder = true;
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Keep at least two users in the room, even when spam is off (currently "
                        + (m.murder ? "murder-happy" : "revive-happy") + ")";
            }
        });

        addAdminCommand("revive", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.murder = false;
            }
        });

        addAdminCommand("beg", new Command("beg|serve", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.captchasPublic = true;
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Notify users of captchas to solve (currently "
                        + (m.captchasPublic ? "ready to beg" : "ready to serve") + ")";
            }
        });

        addAdminCommand("serve", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.captchasPublic = false;
            }
        });

        addAdminCommand("flood", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                if (args.length >= 2) {
                    if (args[0].matches("^[0-9]+$")) {
                        m.floodSize = Integer.parseInt(args[0]);
                    }
                    if (args[1].matches("^[0-9]+$")) {
                        UserConnection.setFloodTime(Integer.parseInt(args[1]));
                    }
                }
                target.schedTell("Flood limit: " + m.floodSize + " messages in " + UserConnection.getFloodTime() + " ms");
            }
        });

        addAdminCommand("ban", new Command("ban REGEX", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.ban(argsToString(0, args));
            }
        });

        addAdminCommand("banspan", new Command("banspan COUNT", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.banSpan = Integer.valueOf(args[0]);
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Set bans to apply for a user's first COUNT messages (currently " + m.banSpan + ")";
            }
        });

        addAdminCommand("bans", new Command(null, "List bans") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                StringBuilder list = new StringBuilder();
                for (String ban : m.getBans()) {
                    list.append("\nban ").append(ban);
                }
                if (list.length() > 0) {
                    target.schedTell(list.substring(1));
                }
            }
        });

        addAdminCommand("clearbans", new Command(null, "Remove all bans") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.clearBans();
            }
        });

        addAdminCommand("banmute", new Command("banmute FREQ", null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                double f = Double.valueOf(args[0]);
                if (f >= 0 && f <= 1) {
                    m.muteBotFreq = f;
                } else {
                    target.schedTell("Must be between 0 and 1");
                }
            }

            @Override
            public String getHelpString(Multi m, UserConnection target) {
                return "Set the fraction of bans to be mutes to FREQ (currently " + m.muteBotFreq + ")";
            }
        });

        addAdminCommand("massacre", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.massacre();
            }
        });

        addAdminCommand("loadrc", new Command("loadrc (FILE)", "Run console commands from the FILE. Beware of recursion!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                loadRc(m, args.length > 0 ? args[0] : null, target);
            }
        });

        addAdminCommand("deify", new Command("deify|undeify", "Make that person a god!") {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.deify(target, args[0], true);
            }
        });

        addAdminCommand("undeify", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                m.deify(target, args[0], false);
            }
        });

        addAdminCommand("password", new Command(null, null) {
            @Override
            public void process(Multi m, String[] args, UserConnection target) {
                //if (target != consoleDummy) { how about telling that always?
                m.tellAdmin(target + " has changed the admin password!");
                //}
                m.password = args[0];
            }
        });

        sanityCheck();
    }

    private static void sanityCheck() throws RuntimeException {
        for (Entry<String, Command> entry : adminCommands.entrySet()) {
            if (entry.getValue() == null) {
                throw new RuntimeException("[sc] Found an admin command with no body in adminCommands!");
            }
            if (entry.getKey().contains(" ")) {
                throw new RuntimeException("[sc] Found an admin command with a space in it!");
            }
            /*
            try {
                if (entry.getValue().getHelpString(null, null) == null) {
                    System.err.println("[sc] Found a command without help: " + entry.getKey());
                }
            }catch(NullPointerException ex){
                //tis fine
            }
            */
        }
    }

    private static void addAdminCommand(String name, Command c) {
        if (adminCommands.containsKey(name)) {
            throw new RuntimeException("[aac] Trying to add ;" + name + "' multiple times!");
        } else {
            if (c.usage == null) {
                c.usage = name;
            }
            adminCommands.put(name, c);
        }
    }

    public static void adminCommand(Multi m, String cmd, UserConnection target) {
        cmd += " ";

        String key = cmd.substring(0, cmd.indexOf(" "));

        String[] args = cmd.substring(cmd.indexOf(" ")).split(" ");

        if (adminCommands.containsKey(key)) {
            adminCommands.get(key).process(m, args, target);
        } else {
            target.schedTell("Unknown command!");
        }

    }

    static void loadRc(Multi m, String name, UserConnection rcuser) {
        if (name == null) {
            name = ".multirc";
        }
        try {
            BufferedReader file = new BufferedReader(new FileReader(name));
            String cmd = null;
            while ((cmd = file.readLine()) != null) {
                Commands.adminCommand(m, cmd, rcuser);
            }
        } catch (FileNotFoundException e) {
            // nop
        } catch (Exception e) {
            System.err.println("Could not load the rc file. The exception follows.");
            e.printStackTrace();
        }
    }
/*
    public static void commandLineX(Multi m, String cmd, UserConnection target) {
        if (cmd.equals("")) {
            // this is a nop. a what? a nop. a what? a nop. oh, a nop.
        } else if (cmd.startsWith("broad")) {
            m.hear(consoleDummy, cmd.substring(6));
        } else if (cmd.startsWith("unichar")) {
            System.out.println(Util.unicodify(cmd.substring(8)));
        } else if (cmd.startsWith("kick")) {
            m.kick(cmd.substring(5));
        } else if (cmd.startsWith("mute")) {
            m.mute(cmd.substring(5));
        } else if (cmd.startsWith("unmute")) {
            m.unmute(cmd.substring(7));
        } else if (cmd.equals("invite")) {
            m.add();
        } else if (cmd.startsWith("qfreq")) {
            double f = Double.valueOf(cmd.substring(6));
            if (f >= 0 && f <= 1) {
                m.qfreq = f;
            } else {
                target.schedTell("Must be between 0 and 1");
            }
        } else if (cmd.equals("inviteq")) {
            m.add(true);
        } else if (cmd.equals("invitec")) {
            m.add(null);
        } else if (cmd.equals("invitep")) {
            m.addPulse();
        } else if (cmd.equals("list")) {
            StringBuilder list = new StringBuilder();
            for (UserConnection u : m.users) {
                synchronized (m.welcomed) {
                    list.append("\n * " + u.getNick() + (u.isPulse() ? " (pulse)"
                            : u.isAccepted() ? m.welcomed.contains(u) ? "" : u.isMuted() ? " (muted)" : " (dead/zombie)" : " (untriaged)"));
                }
            }
            if (list.length() > 0) {
                target.schedTell(list.substring(1));
            }
        } else if (cmd.startsWith("freq")) {
            int f = Integer.valueOf(cmd.substring(5));
            if (f >= 250) {
                m.joinFreq = f;
            } else {
                target.schedTell("Must be 250 or greater (milliseconds, not seconds)");
            }
        } else if (cmd.startsWith("pfreq")) {
            int f = Integer.valueOf(cmd.substring(6));
            m.pulseFreq = f;
        } else if (cmd.equals("verbose")) {
            UserConnection.verbose = true;
        } else if (cmd.equals("terse")) {
            UserConnection.verbose = false;
        } else if (cmd.equals("murder")) {
            m.murder = true;
        } else if (cmd.equals("revive")) {
            m.murder = false;
        } else if (cmd.equals("beg")) {
            m.captchasPublic = true;
        } else if (cmd.equals("serve")) {
            m.captchasPublic = false;
        } else if (cmd.startsWith("flood ")) {
            String[] parts = cmd.split(" ");
            if (parts.length >= 3) {
                if (parts[1].matches("^[0-9]+$")) {
                    m.floodSize = Integer.parseInt(parts[1]);
                }
                if (parts[2].matches("^[0-9]+$")) {
                    UserConnection.setFloodTime(Integer.parseInt(parts[2]));
                }
            }
            target.schedTell("Flood limit: " + m.floodSize + " messages in " + UserConnection.getFloodTime() + " ms");
        } else if (cmd.startsWith("ban ")) {
            m.ban(cmd.substring(4));
        } else if (cmd.startsWith("banspan")) {
            m.banSpan = Integer.valueOf(cmd.substring(8));
        } else if (cmd.equals("bans")) {
            StringBuilder list = new StringBuilder();
            for (String ban : m.getBans()) {
                list.append("\nban " + ban);
            }
            if (list.length() > 0) {
                target.schedTell(list.substring(1));
            }
        } else if (cmd.equals("clearbans")) {
            m.clearBans();
        } else if (cmd.startsWith("banmute")) {
            double f = Double.valueOf(cmd.substring(8));
            if (f >= 0 && f <= 1) {
                m.muteBotFreq = f;
            } else {
                target.schedTell("Must be between 0 and 1");
            }
        } else if (cmd.equals("massacre")) {
            m.massacre();
        } else if (cmd.startsWith("loadrc")) {
            loadRc(m, cmd.length() > 6 ? cmd.substring(7) : null);
        } else if (cmd.startsWith("deify")) {
            m.deify(target, cmd.substring(6), true);
        } else if (cmd.startsWith("undeify")) {
            m.deify(target, cmd.substring(8), false);
        } else if (cmd.startsWith("password")) {
            if (target != consoleDummy) {
                m.tellAdmin(target + " has changed the admin password!");
            }
            m.password = cmd.substring(9);
        } else if (cmd.startsWith("/")) {
            m.command(consoleDummy, cmd);
        } else if (target == consoleDummy && cmd.startsWith("h")) {
            System.err.println(
                    "Valid commands are the slash commands (type /help for info), plus these which don't have slashes:\n"
                    + "   (On lines like [un]mute or beg|serve, the bracketed or second command undoes the main one)\n"
                    + " broad MESSAGE      Broadcast a message\n"
                    + " kick USER          Kick the user immediately\n"
                    + " [un]mute USER      Hide the user from the room at large\n"
                    + " invite             Invite a user, " + m.qfreq + " from question mode\n"
                    + " qfreq FREQ         Set the question mode fraction to FREQ\n"
                    + " inviteq            Invite a user from question mode\n"
                    + " invitec            Invite a user from chat mode\n"
                    + " invitep            Invite a returning user (\"pulse\") from chat mode\n"
                    + " list               List all users, including untriaged\n"
                    + " freq FREQ          Set the spam frequency to FREQ (currently " + m.joinFreq + ") milliseconds\n"
                    + " pfreq FREQ         Set the pulse frequency to FREQ (currently " + m.pulseFreq + ") milliseconds\n"
                    + "   (Limited by spam frequency. -1 to disable pulse)\n"
                    + " verbose|terse      Verbose output, including system messages sent to users (currently "
                    + (UserConnection.verbose ? "verbose" : "terse") + ")\n"
                    + " revive|murder      Keep at least two users in the room, even when spam is off (currently "
                    + (m.murder ? "murder-happy" : "revive-happy") + ")\n"
                    + " beg|serve          Notify users of captchas to solve (currently "
                    + (m.captchasPublic ? "ready to beg" : "ready to serve") + ")\n"
                    + " flood COUNT TIME   Set flood limit to COUNT messages in TIME milliseconds (currently "
                    + m.floodSize + " in " + UserConnection.getFloodTime() + ")\n"
                    + " ban REGEXP         Automatically kick users opening (first 3 lines) with text matching REGEXP\n"
                    + " banspan COUNT      Set bans to apply for a user's first COUNT messages (currently " + m.banSpan + ")\n"
                    + " banmute FREQ       Set the fraction of bans to be mutes to FREQ (currently " + m.muteBotFreq + ")\n"
                    + " bans               List bans\n"
                    + " clearbans          Remove all bans\n"
                    + " loadrc FILE        Run console commands from the FILE. Beware of recursion!\n"
                    + " [un]deify USER     Make the user a remote admin\n"
                    + " password PASSWORD  Set the remote admin password and remove any existing remote admins\n"
                    + "   (Log in remotely using '/password PASSWORD'. Prefix remote admin commands with '/!')\n"
                    + " massacre           Immediately end the chat\n");
        } else {
            target.schedTell("That isn't a command. If on console, type 'help' for a list.\n"
                    + " qfreq " + m.qfreq + "\n"
                    + " freq " + m.joinFreq + "\n"
                    + " pfreq " + m.pulseFreq + "\n"
                    + (UserConnection.verbose ? " verbose" : " terse") + ")\n"
                    + (m.murder ? " murder" : " revive") + ")\n"
                    + (m.captchasPublic ? " beg" : " serve") + "\n"
                    + " flood " + m.floodSize + " " + UserConnection.getFloodTime() + ")\n"
                    + " banspan " + m.banSpan + ")\n"
                    + " banmute " + m.muteBotFreq);
        }
    }
    */
}
