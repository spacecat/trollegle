package anon.trollegle;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;

public class Multi implements Callback {

    List<UserConnection> users = new ArrayList<>();
    private List<UserConnection> informed = new ArrayList<>();
    List<UserConnection> welcomed = new ArrayList<>();

    private List<CaptchaDialog> captchas = new ArrayList<>();
    private Map<UserConnection, CaptchaDialog> userCaptchas = new WeakHashMap<>();
    boolean captchasPublic;

    private Map<UserConnection, Set<UserConnection>> kickVotes = new HashMap<>();

    private final Set<String> bans = new HashSet<>();

    int joinFreq = 5000, pulseFreq = 360007, maxChatting = 9, maxTotal = 25;
    int floodSize = 6;
    private int inactiveBot = 70000, inactiveHuman = 600000, matureHuman = 90000;
    double qfreq = 0.97, muteBotFreq = 0.9;
    private boolean autoJoin;
    private long lastInvite = System.currentTimeMillis();
    private long lastPulse = System.currentTimeMillis();
    private int minInterval = 3000;
    int banSpan = 60;
    boolean murder;
    String password;

    {
        StringBuilder b = new StringBuilder();
        while (b.length() < 16) {
            b.append(UserConnection.RANDID_PIECES.charAt((int) (32 * Math.random())));
        }
        password = b.toString();
    }

    public Multi() {
        new Thread() {
            private ArrayDeque<UserConnection> targets = new ArrayDeque<>();

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(joinFreq() / 2 + (int) (Math.random() * joinFreq()));

                        int pulses = 0;
                        int zombies = 0;

                        synchronized (users) {
                            for (UserConnection u : users) {
                                if (u.isPulse()) {
                                    if (u.idleFor() > pulseFreq) {
                                        u.sendPulse(false);
                                        u.unpulse(autoJoin && Math.random() * (maxChatting + 1) > welcomed.size());
                                    } else {
                                        pulses++;
                                        u.pingStatus();
                                    }
                                    continue;
                                }
                                synchronized (welcomed) {
                                    if ((!u.isAccepted() || !welcomed.contains(u)) && u.idleFor() > inactiveBot
                                            || u.idleFor() > inactiveHuman * (murder && !autoJoin ? 3 : 1)) {
                                        targets.add(u);
                                    }
                                }
                                if (u.idleFor() > inactiveBot && !u.isConnected() && !u.isPulseEver()) {
                                    zombies++;
                                }
                            }
                        }
                        while (!targets.isEmpty()) {
                            kickInactive(targets.pop());
                        }
                        if (zombies > 1) {
                            banned();
                        }

                        if (autoJoin && welcomed.size() < maxChatting && users.size() < maxTotal) {
                            add();
                        }

                        if (pulseFreq >= 0 && pulses == 0 && System.currentTimeMillis() - lastPulse >= pulseFreq) {
                            addPulse();
                            lastPulse = System.currentTimeMillis();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public void massacre() {
        murder = true;
        autoJoin = false;
        pulseFreq = -1;
        tellRoom("This chat is over!");
        UserConnection.sleep(1500);

        ArrayList<UserConnection> toKick;
        synchronized (users) {
            toKick = new ArrayList<>(users);
        }
        for (UserConnection u : toKick) {
            u.sendDisconnect();
            u.dispose();
            remove(u);
            UserConnection.sleep((int) (Math.random() * joinFreq));
            System.out.print(".");
        }
        System.out.println();
    }

    public void banned() {
        System.out.println("Banned! Spam and pulse off");
        System.out.println("(Freqs until now: spam " + joinFreq + " pulse " + pulseFreq);
        autoJoin = false;
        pulseFreq = -1;
        murder = true;
        tellRoom("We've been hard banned! No new users can join. Have fun watching the chat die off.");
    }

    public void ban(String expr) {
        synchronized (bans) {
            bans.add(expr);
        }
    }

    public String[] getBans() {
        synchronized (bans) {
            return bans.toArray(new String[bans.size()]);
        }
    }

    public void clearBans() {
        synchronized (bans) {
            bans.clear();
        }
    }

    public int joinFreq() {
        return welcomed.isEmpty() ? joinFreq : joinFreq * welcomed.size() * welcomed.size();
    }

    public UserConnection userFromName(String name) {
        synchronized (users) {
            for (UserConnection u : users) {
                if (name.equals(u.getNick())) {
                    return u;
                }
            }
        }
        return null;
    }

    public void tellRoom(String message) {
        tellRoom(message, null);
    }

    public void tellRoom(String message, UserConnection excluded) {
        synchronized (users) {
            for (UserConnection u : users) {
                if (u.isReady() && u != excluded) {
                    u.schedTell(message);
                }
            }
        }
    }

    public void tellAdmin(String message) {
        synchronized (users) {
            for (UserConnection u : users) {
                if (u.isReady() && isAdmin(u)) {
                    u.schedTell(message);
                }
            }
        }
    }

    public boolean isAdmin(UserConnection user) {
        if (password == null) {
            return false;
        }
        return password.equals(user.getPassword());
    }

    public void deify(UserConnection source, String targetName, boolean on) {
        UserConnection target = userFromName(targetName);
        if (target == null) {
            source.schedTell("No such user");
        } else if (isAdmin(target) == on) {
            source.schedTell("No change made");
        } else {
            target.setPassword(on ? password : null);
            tellAdmin(source + " has " + (on ? "added " : "removed ") + target + " as an admin.");
            if (!on) {
                target.schedTell(source.getNick() + "has removed you as an admin.");
            }
        }
    }

    public void voteKick(UserConnection nuisance, UserConnection saviour) {
        synchronized (kickVotes) {
            if (!kickVotes.containsKey(nuisance)) {
                kickVotes.put(nuisance, new HashSet<UserConnection>());
            }
            kickVotes.get(nuisance).add(saviour);
        }
        try {
            int eligible = 0;
            synchronized (welcomed) {
                for (UserConnection u : welcomed) {
                    if (u.idleFor() < inactiveHuman && u.age() > matureHuman && u != nuisance) {
                        eligible++;
                    }
                }
            }
            if (kickVotes.get(nuisance).size() * 1.0 / eligible > 0.65) {
                kickVoted(nuisance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unvoteKick(UserConnection nuisance, UserConnection saviour) {
        synchronized (kickVotes) {
            if (!kickVotes.containsKey(nuisance)) {
                return;
            }
            kickVotes.get(nuisance).remove(saviour);
        }
    }

    public void addPulse() {
        UserConnection uc = new UserConnection(this, true, false);
        uc.pulse();
        synchronized (users) {
            uc.start();
            users.add(uc);
        }
    }

    public boolean add(Boolean forceQ) {
        boolean isQ = forceQ == null ? false : forceQ || Math.random() < (qfreq);
        if (!isQ) {
            int texts = 0;
            synchronized (users) {
                for (UserConnection u : users) {
                    if (!u.isQuestionMode() && !u.isPulse()) {
                        texts++;
                    }
                }
            }
            if (texts != 0) {
                isQ = true;
            }
        }
        UserConnection uc = new UserConnection(this, isQ, isQ);
        synchronized (users) {
            uc.start();
            users.add(uc);
        }
        return isQ;
    }

    public boolean add() {
        return add(false);
    }

    private void remove(UserConnection user) {
        if (user == null) {
            System.err.println("warn: removing null user!");
            return;
        }
        user.dispose();
        synchronized (users) {
            users.remove(user);
        }
        synchronized (welcomed) {
            welcomed.remove(user);
        }
        synchronized (informed) {
            informed.remove(user);
        }
        if (user.didLive()) {
            tellRoom(user.getNick() + " has left");
        }
        if (users.isEmpty() && !murder) {
            try {
                Thread.sleep(10000);
                add();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (users.size() == 1 && !murder) {
            synchronized (users) {
                for (UserConnection u : users) {
                    u.schedTell("You are the only one left. We'll try to get you another stranger.");
                }
            }
            try {
                Thread.sleep(2000);
                add();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        synchronized (kickVotes) {
            kickVotes.remove(user);
            for (Set<UserConnection> subset : kickVotes.values()) {
                subset.remove(user);
            }
        }
    }

    private void typing(UserConnection user) {
        if (user.isAccepted()) {
            synchronized (users) {
                for (UserConnection u : users) {
                    if (u != user && u.isReady()) {
                        u.schedSendTyping();
                    }
                }
            }
        }
    }

    public void hear(UserConnection user, String data) {
        if (user.getMsgCount() < 3) {
            if (data.startsWith("| ") && (data.contains(" There are ") || data.contains(" are in the "))
                    || data.contains("dropbox") || data.contains("blasze") || data.contains("omegle69")
                    || data.contains("Hello beauty! :)") || data.contains("sexy female here u?")
                    || data.contains("kera.pm") || data.matches("^(1[89]|2[0-5]) [fF]") && data.length() < 20
                    || data.contains("io-chat") || data.contains("ha.do/") || data.contains("jmp2.in/")
                    || data.toLowerCase().contains("svetlana") && data.toLowerCase().contains("interest")) {
                tellAdmin(user + " marked as spam for opener");
                kickBot(user);
                return;
            }
        }
        if (user.getMsgCount() < banSpan) {
            for (String expr : bans) {
                if (data.matches(expr)) {
                    tellAdmin(user + " marked as spam for pattern " + expr);
                    kickBot(user);
                    return;
                }
            }
        }
        if (!user.isAccepted()) {
            if (user.getMsgCount() > 1) {
                kickBot(user);
            } else {
                user.schedTell("To enter the room, you need to say \"toothpaste\".");
            }
        } else if (data.startsWith("/")) {
            command(user, data);
        } else if (!user.isMuted()) {
            synchronized (users) {
                for (UserConnection u : users) {
                    if (u != user && u.isAccepted() && (!u.isMuted() || !data.matches("(?i).*mute.*"))) {
                        u.schedTell(user.getNick(), data);
                    }
                }
            }
        }

    }

    private void command(UserConnection user, String data) {
        String[] ca = data.split(" ", 2);
        if (ca[0].equalsIgnoreCase("/help")) {
            user.schedTell("Here are " + randomize("", "all") + " the commands "
                    + randomize("", "that ") + randomize("I accept", "this bot accepts") + ".\n"
                    + "/invite\n"
                    + "    Adds another " + randomize("user", "stranger") + " to the chat ("
                    + 100 * qfreq + "% " + randomize("recruited", "taken") + " from question mode)\n"
                    + "/msg USER MESSAGE\n"
                    + "    Sends a private message to the user\n"
                    + "/me MESSAGE\n"
                    + "    Sends an irc-like action message\n"
                    + "/nick NAME\n"
                    + "    Changes your name to the specifed NAME\n"
                    + "/list\n"
                    + "    Shows who's in the chat\n"
                    + "/kick NAME\n"
                    + "    Votes for the user to be kicked. (2/3 of the room needs to do this for the kick to happen.)\n"
                    + "/nokick NAME\n"
                    + "    Withdraws a kick vote\n"
                    + "/spam\n"
                    + "    Starts looking for new strangers (one every " + joinFreq / 1000 + "*num_users^2 secs)\n"
                    + "/nospam\n"
                    + "    Stops looking for new strangers\n"
                    + "    (Spamming is currently " + (autoJoin ? "on" : "off") + ".)\n"
                    + "/fuckyoubecause MESSAGE\n"
                    + "    Sends a message to the administrator of this system\n"
                    + "Curious about how this works? Download teh codes at https://gitlab.com/spacecat/trollegle\n"
                    + "( a fork of http://gitlab.com/jtrygva/trollegle) \n"
                    + "Want to leave and rejoin later? See " + UserConnection.MOTHERSHIP
            );
        } else if (ca[0].equalsIgnoreCase("/rules")) {
            user.schedTell("Every group will have its own norms, so I'm not posting any hard and fast rules "
                    + "here. If you're not sure if some behaviour is halaal, you can hold off until you've lurked "
                    + "enough. Or ask around even. If someone is bothering you, why not talk it out? There's also "
                    + "the /kick command as a last resort.");
        } else if (ca[0].equalsIgnoreCase("/8")) {
            user.schedTell("You've been here for " + minSec(user.age()) + ". /8 count: " + user.lurk());
        } else if (ca[0].equalsIgnoreCase("/id")) {
            String pulse = "none";
            synchronized (users) {
                for (UserConnection u : users) {
                    if (u.isPulse()) {
                        pulse = u.getNick() + " (" + u.getPulseWords() + ", " + u.randid + ", " + minSec(u.idleFor()) + " old)";
                        break;
                    }
                }
            }
            user.schedTell("You: " + user
                    + "\nPulse connection: " + pulse
                    + "\nMothership: " + UserConnection.MOTHERSHIP
            );
        } else if (ca[0].equalsIgnoreCase("/invite") || ca[0].equalsIgnoreCase("/add")) {
            if (user.isMuted()) {
                user.schedTell(user.getNick() + " has invited a new user");
            } else if (System.currentTimeMillis() - lastInvite < minInterval) {
                user.schedTell("Too fast! Try again in a few seconds.");
            } else if (users.size() >= maxTotal || welcomed.size() >= maxChatting) {
                user.schedTell("Chat room is full.");
            } else {
                lastInvite = System.currentTimeMillis();
                if (add()) {
                    tellRoom(user.getNick() + " has invited a new user");
                } else {
                    tellRoom(user.getNick() + " has invited a new user (requesting toothpaste - hold on)");
                }
            }
        } else if (ca[0].equalsIgnoreCase("/msg") || ca[0].equalsIgnoreCase("/pm")) {
            if (ca.length < 2) {
                user.schedTell("You need to give me a user and a message. Syntax: /msg USER MESSAGE");
            } else {
                String[] p = ca[1].split(" ", 2);
                System.out.println("Message command components: " + Arrays.toString(p));
                if (p.length == 2) {
                    UserConnection dest = userFromName(p[0]);
                    if (dest == null) {
                        user.schedTell("No such user - type /list to see who's online.");
                    } else {
                        if (!user.isMuted()) {
                            dest.schedTell("(private) " + user.getNick(), p[1]);
                        }
                        user.schedTell("message sent");
                    }
                } else {
                    user.schedTell("You need to give me both a user and a message. Syntax: /msg USER MESSAGE");
                }
            }
        } else if (ca[0].equalsIgnoreCase("/me")) {
            if (ca.length < 2) {
                user.schedTell("You need to give me a message to send. Syntax: /me MESSAGE");
            } else if (user.isMuted()) {
                user.ircMe(user.getNick(), ca[1]);
            } else {
                synchronized (users) {
                    for (UserConnection u : users) {
                        if (u.isAccepted()) {
                            u.ircMe(user.getNick(), ca[1]);
                        }
                    }
                }
            }
        } else if (ca[0].equalsIgnoreCase("/fuckyoubecause") || ca[0].equalsIgnoreCase("/heyadmin")) {
            if (ca.length == 1) {
                tellAdmin(user + " is calling you (" + ca[0] + ")");
            } else {
                tellAdmin(user + " is calling you ( " + ca[0] + "): " + ca[1]);
            }
            if (user.isMuted()) {
                user.schedTell("The admin has been called. Might be away from the keyboard though, in which case tough luck.");
            } else {
                synchronized (users) {
                    for (UserConnection u : users) {
                        u.schedTell("The admin has been called. Might be away from the keyboard though, in which case tough luck.");
                    }
                }
                System.out.println("\n\n\nSomeone fucking called you!\n\n\n");
            }
        } else if (ca[0].equalsIgnoreCase("/nick") || ca[0].equalsIgnoreCase("/name")) {
            if (ca.length < 2) {
                user.schedTell("You need to give me a name. Syntax: /nick NAME");
            } else if (user.isMuted()) {
                user.schedTell("'" + user.getNick() + "' is now known as '" + ca[1] + "'");
            } else {
                ca[1] = ca[1].replace(" ", "-").replace("\n", "").replace("\r", "").replace("\b", "");
                boolean go = true;
                if (ca[1].toLowerCase().matches("adm[iı]n")) {
                    go = false;
                }

                if (go) {
                    tellRoom("'" + user.getNick() + "' is now known as '" + ca[1] + "'");
                    user.setNick(ca[1]);
                } else {
                    user.schedTell("Someone already has that name. Try another one.");
                }
            }
        } else if (ca[0].equalsIgnoreCase("/cap")) {
            if (ca.length < 2 || !userCaptchas.containsKey(user)) {
                CaptchaDialog cd = captchas.get((int) (Math.random() * captchas.size()));
                synchronized (userCaptchas) {
                    userCaptchas.put(user, cd);
                }
                for (String line : cd.getArt().split("\n ")) {
                    user.schedTell(line);
                }
                user.schedTell("Type /cap plus the solution to submit. /cap alone tries to get another captcha.");
            } else {
                CaptchaDialog cd = userCaptchas.get(user);
                try {
                    if (cd.send(ca[1])) {
                        synchronized (captchas) {
                            captchas.remove(cd);
                        }
                        synchronized (userCaptchas) {
                            userCaptchas.remove(user);
                        }
                        user.schedTell("win");
                    } else {
                        user.schedTell("fail");
                        cd.update();
                    }
                } catch (Exception e) {
                    user.schedTell("Something went wrong. Try doing a /cap again.");
                    e.printStackTrace();
                };
            }
        } else if (ca[0].equalsIgnoreCase("/list") || ca[0].equalsIgnoreCase("/who")) {
            user.schedTell("There are " + welcomed.size() + " users in the room:");
            StringBuilder list = new StringBuilder("");
            synchronized (users) {
                for (UserConnection u : users) {
                    if (u == user) {
                        list.append("\n    ").append(u.getNick()).append(" <- you");
                    } else if (u.isReady()) {
                        list.append("\n    ").append(u.getNick());
                    }
                }
            }
            if (list.length() > 1) {
                user.schedTell(list.substring(0));
            }
        } else if (ca[0].equalsIgnoreCase("/spam")) {
            int joinRestSec = joinFreq() * 3 / 2000;
            int joinFreqSec = joinFreq() / 1000;
            if (autoJoin) {
                if (welcomed.size() >= maxChatting) {
                    user.schedTell("Spamming is paused because the room is full (max " + maxChatting + " users). It will resume once someone leaves.");
                } else if (users.size() >= maxTotal) {
                    user.schedTell("Spamming is paused because of lingering connections. It will resume within " + joinRestSec + " seconds.");
                } else {
                    user.schedTell("Spamming is already on. Current rate is " + joinFreqSec + " seconds.");
                }
            } else if (user.isMuted()) {
                user.schedTell(user.getNick() + " has turned spamming on.");
            } else {
                autoJoin = true;
                tellRoom(user.getNick() + " has turned spamming on.");

                if (welcomed.size() >= maxChatting) {
                    tellRoom("Spamming is paused because the room is full (max " + maxChatting + " users). It will resume once someone leaves.");
                } else if (users.size() >= maxTotal) {
                    tellRoom("Spamming is paused because of lingering connections. It will resume within " + joinRestSec + " seconds.");
                }
            }
        } else if (ca[0].equalsIgnoreCase("/nospam")) {
            if (!autoJoin) {
                user.schedTell("Spamming is already off.");
            } else if (user.isMuted()) {
                user.schedTell(user.getNick() + " has turned spamming off.");
            } else {
                autoJoin = false;
                tellRoom(user.getNick() + " has turned spamming off.");
            }
        } else if (ca[0].equalsIgnoreCase("/kick")) {
            if (ca.length < 2) {
                user.schedTell("You need to give me a user to kick. Syntax: /kick USER");
            } else if (welcomed.size() == 1) {
                user.schedTell("There isn't anyone there to kick.");
            } else if (userFromName(ca[1]) == null) {
                user.schedTell("No user exists with that name. Maybe they left of their own accord?");
            } else if (user.age() < matureHuman) {
                user.schedTell("You aren't old enough to vote yet. Wait a minute or two.");
            } else if (user.isMuted()) {
                user.schedTell(user.getNick() + " has voted to kick " + ca[1] + " from the room.");
            } else {
                tellRoom(user.getNick() + " has voted to kick " + ca[1] + " from the room.");
                voteKick(userFromName(ca[1]), user);
            }
        } else if (ca[0].equalsIgnoreCase("/dontkick") || ca[0].equalsIgnoreCase("/unkick") || ca[0].equalsIgnoreCase("/nokick")) {
            if (ca.length < 2) {
                user.schedTell("You need to give me a user to forgive. Syntax: /dontkick USER");
            } else if (welcomed.size() == 1) {
                user.schedTell("There isn't anyone there to kick.");
            } else if (userFromName(ca[1]) == null) {
                user.schedTell("No user exists with that name, maybe they're already gone");
            } else if (user.isMuted()) {
                user.schedTell(user.getNick() + " has withdrawn their vote to kick " + ca[1] + " from the room.");
            } else {
                unvoteKick(userFromName(ca[1]), user);
                tellRoom(user.getNick() + " has withdrawn their vote to kick " + ca[1] + " from the room.");
            }
        } else if (ca[0].equalsIgnoreCase("/password")) {
            user.setPassword(ca[1]);
            user.schedTell("I won't tell you if that's right.");    //<--- leems segit
        } else if (ca[0].startsWith("/!")) {
            if (isAdmin(user)) {
                Commands.adminCommand(this, ca.length == 1 ? ca[0].substring(2) : ca[0].substring(2) + " " + ca[1], user);
            } else {
                user.schedTell("Wrong password.");                  //<--- leems segit
            }
        } else {
            user.schedTell("That isn't a known command. Type /help for more info.");
        }
    }

    private String[] youHave = {"You've ", "You have ",};
    private String[] stumbled = {"stumbled upon ", "come upon ",
        "been chosen to take part in ", "been selected to enter ",};
    private String[] entered = {"stumbled upon ", "come upon ",
        "entered  ", "found ",};
    private String[] roving = {"a roving ", "a wandering ", "a ", "an Omegle ",};
    private String[] groupChat = {"group chat. ", "groupchat. ", "multi-user chat. ", "chat room. ", "chatroom. ",};
    private String[] doNote = {"Do note, ", "Note that ", "Please be aware that ", "Do be aware that ",};
    private String[] notThePlace = {"this is not the place to ", "this is not a place to ",};
    private String[] flauntGenitalia = {"flaunt the fitness of your sex organs", "brag about your sexual prowess",
        "advertise your physical attractiveness", "display your undoubtedly amazing genitalia",};
    private String[] orAnything = {" or somesuch. ", " or anything. ", " or anything like that. ", ". ",};

    private String[] toothpaste = {"To enter the __room__, you need to say \"toothpaste\". ",
        "Type \"toothpaste\" to enter the __room__.", "Enter the __room__ by typing \"toothpaste\". ",
        "To enter the __room__, say the word \"toothpaste\" as a show of human conformity. ",
        "To enter the __room__, say the word \"toothpaste\". ",};
    private String[] room = {"room", "chat room", "chatroom", "chat",};

    private String[] nobody = {"No one else has entered the __room__ yet", "The __room__ is still empty",
        "Nobody else has joined yet",};
    private String[] so = {", so ", " - ",};
    private String[] boring = {"be prepared for a bore.", "you may need to wait a while.",
        "you'll need to be patient.",};

    private String[] youAre = {"You are ", "You happen to be ", "You're ",};
    private String[] firstUser = {"the first one ", "the first user ",};
    private String[] inTheRoom = {"in the __room__. ", "in this __room__. ", "here. ", "to join. ",};
    private String[] holdOn = {"Hold on", "You'll have to wait a while",};
    private String[] lookFor = {", we're looking for ", " while we look for ", ", we're trying to find ",};
    private String[] someoneElse = {"someone else. \n", "another user. \n", "more users. \n", "another stranger. \n",};

    private String[] nOthers = {"There are __num__ __others__ in the __room__",
        "__num__ __others__ are in the __room__",};
    private String[] others = {"users", "others", "other users", "other strangers", "other losers",};

    private String randomize(String... choices) {
        return choices[(int) (Math.random() * choices.length)];
    }

    private String randomize(String[]  
        ... choices) {
		StringBuilder b = new StringBuilder();
        for (String[] set : choices) {
            b.append(randomize(set));
        }
        return b.toString();
    }

    private void welcome(UserConnection user) {
        if (user.isAccepted()) {
            synchronized (welcomed) {
                if (welcomed.contains(user)) {
                    return;
                }
            }

            boolean isInformed = false;
            synchronized (informed) {
                if (informed.contains(user)) {
                    isInformed = true;
                }
            }
            boolean separateFirstLine = Math.random() > 0.66;
            String firstLine = randomize("This is ", "You're in ", randomize(youHave, entered)) + randomize(roving, groupChat);
            if (!isInformed && separateFirstLine) {
                user.schedTell(firstLine);
            }

            if (welcomed.size() < 1) {
                user.schedTell(randomize(youAre, firstUser, inTheRoom, holdOn, lookFor, someoneElse)
                        .replace("__room__", randomize(room))
                        + "(Your username will be: '" + user.getNick() + "')");
                if (!murder) {
                    add();
                }
            } else {
                StringBuilder otherUsers = new StringBuilder();
                synchronized (users) {
                    for (UserConnection u : users) {
                        if (u != user && u.isReady()) {
                            otherUsers.append(", ").append(u.getNick());
                            if (user.getQuestion() != null) {
                                u.schedTell(user.getNick() + " has joined with question: '" + user.getQuestion() + "'");
                            } else {
                                u.schedTell(user.getNick() + " has joined");
                            }
                        }
                    }
                }
                boolean showUsers = otherUsers.length() > 2 && Math.random() > 1d / 3;
                String countLine = randomize(nOthers).replace("__room__", randomize(room)).replace("__others__",
                        randomize(others)).replace("__num__", String.valueOf(welcomed.size())) + (showUsers ? ": " : ".");
                if (!separateFirstLine && !isInformed && Math.random() > 0.5) {
                    user.schedTell(firstLine + countLine);
                } else {
                    user.schedTell(countLine);
                }
                if (showUsers) {
                    if (Math.random() > 0.5) {
                        user.schedTell("    " + otherUsers.substring(2));
                    } else {
                        for (String otherUser : otherUsers.substring(2).split(", ")) {
                            user.schedTell("    " + otherUser);
                        }
                    }
                }
                user.schedTell("'" + user.getNick() + "' is your username. Everyone sees it in front of the messages you send.");
            }
            user.schedTell("Type '/nick NAME' to change your name, or '/help' to see the commands you can use.");
            synchronized (welcomed) {
                welcomed.add(user);
            }
        } else {
            if (informed.contains(user)) {
                return;
            }
            UserConnection.sleep((int) (Math.random() * 700));

            String stumbledLine = randomize(youHave, stumbled, roving, groupChat);
            String flauntLine = randomize(doNote, notThePlace, flauntGenitalia, orAnything);
            String pasteLine = randomize(toothpaste).replace("__room__", randomize(room));

            if (Math.random() < 0.5) {
                if (Math.random() < 0.5) {
                    user.schedTell(stumbledLine);
                    user.schedTell(flauntLine);
                    user.schedTell(pasteLine);
                } else {
                    user.schedTell(stumbledLine + flauntLine);
                    user.schedTell(pasteLine);
                }
            } else {
                if (Math.random() < 0.5) {
                    user.schedTell(stumbledLine);
                    user.schedTell(flauntLine + pasteLine);
                } else {
                    user.schedTell(stumbledLine);
                    user.schedTell(pasteLine + flauntLine);
                }
            }

            if (welcomed.size() < 2) {
                user.schedTell("(" + randomize(nobody, so, boring).replace("__room__", randomize(room)) + ")");
            }
            synchronized (informed) {
                informed.add(user);
            }
        }

    }

    private void kickVoted(UserConnection u) {
        kick(u, "(The room voted to kick this user)");
    }

    private void kickInactive(UserConnection u) {
        kick(u, "(The user was inactive for too long)");
    }

    private void kickBot(UserConnection u) {
        if (Math.random() < muteBotFreq) {
            mute(u);
        } else {
            kick(u, "(Their first messages looked like spam)");
        }
    }

    private void kickFlood(UserConnection u) {
        kick(u, "(They flooded the chat. Limit is " + floodSize + " messages in "
                + UserConnection.getFloodTime() / 1000.0 + " seconds)");
    }

    private void kick(UserConnection u, String message) {
        if (u == null) {
            System.err.println("not a user (tried to kick: " + message + ")");
        } else {
            u.sendDisconnect();
            u.dispose();
            remove(u);
            System.out.println(u.getNick() + " has been kicked: " + message);
            if (u.didLive()) {
                tellRoom(message);
            }
        }
    }

    void kick(String name) {
        UserConnection u = userFromName(name);
        if (u == null) {
            System.err.println(name + " isn't a user");
        } else {
            kick(u, "(The user was kicked by an admin)");
        }
    }

    private void mute(UserConnection u) {
        synchronized (welcomed) {
            welcomed.remove(u);
        }
        if (!u.isMuted()) {
            System.err.println("Muted: " + u.getNick());
            if (isAdmin(u)) {
                u.schedTell("You have been muted!");
            }
        }
        u.setMuted(true);
    }

    void mute(String name) {
        UserConnection u = userFromName(name);
        if (u == null) {
            System.err.println(name + " isn't a user");
        } else {
            mute(u);
        }
    }

    private void unmute(UserConnection u) {
        synchronized (welcomed) {
            if (!welcomed.contains(u)) {
                welcomed.add(u);
            }
        }
        if (u.isMuted()) {
            System.err.println("Unmuted: " + u.getNick());
        }
        u.setMuted(false);
    }

    void unmute(String name) {
        UserConnection u = userFromName(name);
        if (u == null) {
            System.err.println(name + " isn't a user");
        } else {
            unmute(u);
        }
    }

    public void callback(UserConnection user, String method, int data) {
        if (method.equals(UserConnection.FLOOD)) {
            if (data > floodSize) {
                if (user.age() < matureHuman || user.isWarned()) {
                    kickFlood(user);
                } else {
                    user.warn();
                    tellRoom("The flood limit is " + floodSize + " messages in "
                            + UserConnection.getFloodTime() / 1000.0 + " seconds. " + user.getNick()
                            + ", you will be kicked if you continue.");
                }
            }
        }
    }

    @Override
    public void callback(UserConnection user, String method, String data) {
        //System.out.println("callback: '" + user + "', '" + method + "', '" + data + "'");
        if (method.equals("captcha")) {
            autoJoin = false;
            System.out.println("\n\nCaptcha received - spamming turned off\n\n");
            System.out.println("(Freqs until now: spam " + joinFreq + " pulse " + pulseFreq);
            pulseFreq = -1;
            murder = true;
            if (captchasPublic) {
                tellRoom("We've been captcha'd! Spamming is now off. Type /cap to try to solve one.");
            } else {
                tellRoom("We've been captcha'd! No new users can join, unless the admin manually solves captchas. All you can do is watch the room die.");
            }

            synchronized (captchas) {
                captchas.add(new CaptchaDialog(data, user.getID()));
            }
        } else if (method.equals("ban")) {
            banned();
        } else if (method.equals("died")) {
            remove(user);
        } else if (method.equals("disconnected")) {
            remove(user);
        } else if (method.equals("typing")) {
            typing(user);
        } else if (method.equals("message")) {
            hear(user, data);
        } else if (method.equals("connected") || method.equals("accepted")) {
            welcome(user);
        } else if (method.equals("pulse success")) {
            addPulse();
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD '" + method + "'\n\n\n");
        }
    }

    private static Multi m;
    private static UserConnection consoleDummy;

    public static void main(String[] args) {
        m = new Multi();
        consoleDummy = new UserConnection(m) {
            @Override
            public void sendAction(String s, String t) {
                if (s.equals("send")) {
                    System.err.println(t);
                } else {
                    System.err.println("[Dummy-" + s + "]" + t);
                }
            }

            @Override
            public void schedTell(String str) {
                tell(str);
            }

            @Override
            public void schedTell(String one, String two) {
                tell(one, two);
            }

            {
                setNick("Admin");
                dummy();
            }
        };
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Commands.loadRc(m, null, consoleDummy);
        while (true) {
            System.err.print("> ");
            try {
                String cmd = in.readLine();
                if(cmd.startsWith("/")){
                    m.command(consoleDummy, cmd);
                }else{
                    Commands.adminCommand(m, cmd, consoleDummy);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static String minSec(long time) {
        time /= 1000;
        long min = time / 60;
        if (min != 0) {
            return min + " min " + time % 60 + " s";
        }
        return time + " s";
    }
    
    UserConnection[] listUsers(){
        //TODO: add code
        return null;
    }
}