package anon.trollegle;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static String[] loadStringsFromResource(String resourcePath) throws IOException {

        InputStream is;
        Scanner scanner;

        ArrayList<String> read = new ArrayList<>();

        {
            is = Util.class.getResource(resourcePath).openStream();
            scanner = new Scanner(is);

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                read.add(line);
            }

            scanner.close();
            is.close();
        }

        return read.toArray(new String[read.size()]);

    }

    public static String unicodify(String quoted) {
        Pattern pat = Pattern.compile("([^\\\\]|^)\\\\u+([a-fA-F0-9]{4})");
        while (pat.matcher(quoted).find()) {
            Matcher mat = pat.matcher(quoted);
            StringBuffer result = new StringBuffer();
            while (mat.find()) {
                char chr = (char) Integer.parseInt(mat.group(2), 16);
                mat.appendReplacement(result, mat.group(1) + chr);
            }
            mat.appendTail(result);
            quoted = result.toString();
        }
        return quoted;
    }

}
