package anon.trollegle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserConnection extends Thread {

    private static final Logger LOGGER = Logger.getLogger(UserConnection.class.getName());

    private static String[] randids;
    public static final int NUM_RANDIDS = 3;
    public static final String MOTHERSHIP = "http://bellawhiskey.ca/trollegle/";
    public static final String RANDID_PIECES = "23456789ABCDEFGHJKLMNPQRSTUVWXYZ";

    static {
        randids = new String[NUM_RANDIDS];
        for (int i = 0; i < NUM_RANDIDS; i++) {
            StringBuilder b = new StringBuilder();
            while (b.length() < 8) {
                b.append(RANDID_PIECES.charAt((int) (32 * Math.random())));
            }
            randids[i] = b.toString();
        }
    }
    private static long floodTime = 7500;
    private static long warnTime = 300000;
    public static final String FLOOD = "flood";

    public final String randid = randids[(int) (Math.random() * randids.length)];
    private String pulseWord, pulseName;
    private String password;
    private String id = "";
    private String nick, question;
    private Callback callback;
    private int msgCount;
    private final int server = (int) (Math.random() * 16) + 1;
    private boolean connected, done, accepted, questionMode, pulse, pulseEver, lived, muted;
    private final Queue<String> tellQueue = new ArrayDeque<>();
    private final Set<String> nicksUsed = new HashSet<>();
    private long lastActive = System.currentTimeMillis();
    private long lastRollover = System.currentTimeMillis();
    private long lastWarning;
    private long birth = Long.MAX_VALUE;
    private int floodMeter;
    private int lurkCount;

    static boolean verbose;

    private static String[] names = null;
    private static String[] words = null;
    
    static {
        try {
            names = Util.loadStringsFromResource("/data/names");
            words = Util.loadStringsFromResource("/data/words");
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Failed to load names/words :v", ex);
        }
    }

    public UserConnection(Callback callback) {
        this.callback = callback;
        
        synchronized (this.nicksUsed) {
            do {
                this.nick = names[(int) (Math.random() * names.length)];
            } while (this.nicksUsed.contains(this.nick));
            this.nicksUsed.add(this.nick);
        }
        
        generatePulse();
    }

    public UserConnection(Callback callback, boolean accepted, boolean questionMode) {
        this(callback);
        
        this.accepted = accepted;
        this.questionMode = questionMode;
    }

    public int getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(int c) {
        msgCount = c;
    }

    public String getID() {
        return id;
    }

    public void warn() {
        lastWarning = System.currentTimeMillis();
    }

    public boolean isWarned() {
        return System.currentTimeMillis() - lastWarning < warnTime;
    }

    public long idleFor() {
        return System.currentTimeMillis() - lastActive;
    }

    public long age() {
        return System.currentTimeMillis() - birth;
    }

    public int lurk() {
        return ++lurkCount;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        synchronized (nicksUsed) {
            if (nicksUsed.contains(nick)) {
                throw new IllegalArgumentException("nick already used");
            }
            nicksUsed.remove(this.nick);
            this.nick = nick;
            nicksUsed.add(nick);
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQuestion() {
        return question;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public boolean isMuted() {
        return muted;
    }

    public boolean isReady() {
        return connected && accepted;
    }

    public boolean didLive() {
        return accepted && lived;
    }

    public boolean isQuestionMode() {
        return questionMode;
    }

    public boolean isPulse() {
        return pulse;
    }

    public boolean isPulseEver() {
        return pulseEver;
    }

    public String getPulseWords() {
        return pulseWord + " " + pulseName;
    }

    @Override
    public String toString() {
        return getNick() + " (" + (isPulseEver() ? getPulseWords()
                : isQuestionMode() ? getQuestion() : "text mode") + ", " + randid + ", "
                + Multi.minSec(age()) + " old)";
    }

    public UserConnection pulse() {
        this.pulse = true;
        this.pulseEver = true;
        return this;
    }

    public void unpulse(boolean convert) {
        pulse = false;
        if (convert) {
            accepted = false;
            sendAction("stoplookingforcommonlikes", null);
        }
    }

    protected UserConnection dummy() {
        connected = true;
        lived = true;
        accepted = true;
        birth = System.currentTimeMillis();
        msgCount = 5;
        return this;
    }

    void fillHeaders(URLConnection conn, String s) {
        s = " " + s + " ";
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux i686; rv:43.0) Gecko/20100101 Firefox/43.0");
        if (s.contains(" json ")) {
            conn.setRequestProperty("Accept", "application/json");
        } else {
            conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
        }
        if (s.contains(" post ")) {
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        }
        conn.setRequestProperty("Accept-Language", "en-US;en;q=0.5");
        conn.setRequestProperty("Origin", "http://www.omegle.com");
        conn.setRequestProperty("Referer", "http://www.omegle.com/");
        /*conn.setRequestProperty("Cookie", "randid=" + randid);
         conn.setRequestProperty("Cookie", "__cfduid=dfae36274feb8aa94d2e83a69086255d91470913354");
         conn.setRequestProperty("Cookie", "fblikes=0");*/
    }

    public void pingStatus() {
        try {
            URL homeUrl = new URL("http://front" + server + ".omegle.com/status?nocache=" + Math.random() + "&randid=" + randid);
            URLConnection conn = homeUrl.openConnection();
            fillHeaders(conn, "json");
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            String line = rd.readLine();
            logVerbose("Status for pulse " + nick + " (front " + server + "): " + line);
            rd.close();
        } catch (Exception ex) {
            log("Failed to get status for " + nick);
            if (verbose) {
                ex.printStackTrace();
            }
        }

    }

    public String establishChat() {
        String line = null;
        msgCount = 0;
        try {
            String topics = pulse ? "&topics=[\"" + pulseWord + "\",\"" + pulseName + "\"]" : "&topics=['groupchat','irc','chatroom','groupchats','chatrooms','IT','programming','government']".replaceAll("'", "\"");
            if (!questionMode) {
                logVerbose("!! Listening on front" + server);
            }
            //topics = URLEncoder.encode(topics);
            URL url = new URL("http://front" + server + ".omegle.com/start?rcs=1&firstevents=1&spid=&randid=" + randid + (questionMode ? "&wantsspy=1" : topics) + "&lang=en");
            //if (questionMode) questionMode = false;
            logVerbose("Starting chat for " + nick);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            fillHeaders(conn, "json post");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write("");
            wr.flush();
            logVerbose("Connection established for " + nick);
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            line = rd.readLine();
            //System.out.println("line=\""+line+"\"");
            wr.close();
            rd.close();
            if (pulse) {
                sendPulse(true);
            }
        } catch (Exception ex) {
            connected = false;
            callback.callback(this, "died", ex.getMessage());
            if (verbose) {
                ex.printStackTrace();
            }
        }
        id = line.replaceAll(".*\"clientID\": *\"([^\"]+)\".*", "$1");
        if (line.trim().equals("")) {
            callback.callback(this, "ban", null);
        } else {
            handleReply(line);
        }
        //System.out.println("id: " + id);
        return id;
    }

    protected void sendPulse(boolean on) {
        try {
            URL homeUrl = new URL(MOTHERSHIP + pulseWord + "," + pulseName + "-" + (on ? "1" : "0"));
            URLConnection conn = homeUrl.openConnection();
            conn.setDoOutput(true);
            fillHeaders(conn, "json post");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write("");
            wr.flush();
            BufferedReader rd
                    = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
            String line = rd.readLine();
            log("Pulse for " + nick + " (front " + server + "): " + line);
            wr.close();
            rd.close();
        } catch (Exception ex) {
            log("Failed to send pulse for " + nick);
            if (verbose) {
                ex.printStackTrace();
            }
        }
    }

    protected void sendAction(String action, String msg) {
        if (msgCount == -1 || id.isEmpty()) {
            return;
        }
        try {
            URL url = new URL(new StringBuilder("http://front" + server + ".omegle.com/").append(URLEncoder.encode(action, "UTF-8")).toString());
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            fillHeaders(conn, "post");
            OutputStreamWriter wr
                    = new OutputStreamWriter(conn.getOutputStream());
            if (msg != null) {
                wr.write(new StringBuilder("msg=").append(URLEncoder.encode(msg, "UTF-8")).append("&id=").append(id).toString());
            } else {
                wr.write(new StringBuilder("id=").append(id).toString());
            }
            wr.flush();
            wr.close();
            BufferedReader rd;
            try {
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } catch (java.io.FileNotFoundException e) {
                rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                log(nick + " UserConnection 404");
            }
            rd.readLine();
            rd.close();
        } catch (Exception ex) {
            log(new StringBuilder(id).append(" had exception: ").append(ex.getMessage()).toString());
            ex.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        sendAction("send", message);
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static long getFloodTime() {
        return floodTime;
    }

    public static void setFloodTime(long floodTime) {
        UserConnection.floodTime = floodTime;
    }

    public void log(Object message) {
        long time = System.currentTimeMillis();
        
        System.out.printf("%02d:%02d ", time / 60000 / 60 % 24, time / 60000 % 60);
        System.out.println(message);
    }

    public void logVerbose(Object message) {
        if (verbose) {
            log(message);
        }
    }

    public void tell(String message) {
        if (getClass() == UserConnection.class) {
            log("[" + nick + " is being told]" + message);
        }
        sendAction("send", "| " + message);
        sleep(75);
    }

    public void schedTell(String message) {
        synchronized (tellQueue) {
            tellQueue.add("| " + message);
        }
    }

    public void tell(String from, String message) {
        sendAction("send", "[" + from + "] " + message);
        sleep(75);
    }

    public void schedTell(String from, String message) {
        synchronized (tellQueue) {
            tellQueue.add("[" + from + "] " + message);
        }
    }

    public void ircMe(String from, String message) {
        synchronized (tellQueue) {
            tellQueue.add(" * " + from + " " + message);
        }
    }

    public void sendDisconnect() {
        sendAction("disconnect", null);
    }

    public void sendTyping() {
        sendAction("typing", null);
    }

    public void schedSendTyping() {
        synchronized (tellQueue) {
            tellQueue.add("/internaltyping");
        }
    }

    public void dispose() {
        if (pulse) {
            sendPulse(false);
        }
        done = true;
    }



    public void run() {
        id = establishChat();
        new Thread() {
            public void run() {
                while (!done) {
                    UserConnection.sleep(75);
                    if (!connected) {
                        synchronized (tellQueue) {
                            tellQueue.clear();
                        }
                        continue;
                    }
                    while (!tellQueue.isEmpty()) {
                        if (tellQueue.peek().equals("/internaltyping")) {
                            sendTyping();
                            synchronized (tellQueue) {
                                tellQueue.poll();
                            }
                        } else {
                            if (!tellQueue.peek().startsWith("[")) {
                                logVerbose("[" + nick + " is being told]" + tellQueue.peek());
                                UserConnection.sleep(75 + (int) (20 * Math.log(tellQueue.peek().length())));
                                sendTyping();
                                UserConnection.sleep(75 + (int) (35 * Math.log(tellQueue.peek().length())));
                            } else {
                                sendTyping();
                                UserConnection.sleep(75 + (int) (30 * Math.log(tellQueue.peek().length())));
                            }
                            String message = "";
                            synchronized (tellQueue) {
                                message = tellQueue.poll();
                            }
                            sendAction("send", message);
                        }
                        UserConnection.sleep(75);
                    }
                }
            }
        }.start();
        boolean rcvd = true;
        while (rcvd && !done) {
            try {
                rcvd = false;
                URL url = new URL("http://front" + server + ".omegle.com/events");
                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                fillHeaders(conn, "post json");
                OutputStreamWriter wr
                        = new OutputStreamWriter(conn.getOutputStream());
                wr.write(new StringBuilder("id=").append(id).toString());
                wr.flush();
                BufferedReader rd
                        = (new BufferedReader(new InputStreamReader(conn.getInputStream())));
                String line;
                while ((line = rd.readLine()) != null) {
                    if (!line.equals("null")) {
                        rcvd = true;
                        handleReply(line);
                    }
                }
                wr.close();
                rd.close();
                Thread.sleep(200);
            } catch (Exception ex) {
                connected = false;
                if (callback != null) {
                    callback.callback(this, "died", ex.getMessage());
                }
            }
        }
        synchronized (nicksUsed) {
            nicksUsed.remove(nick);
        }
    }

    private void checkFlood() {
        if (lastActive - lastRollover < floodTime) {
            callback.callback(this, FLOOD, floodMeter++);
        } else {
            lastRollover = lastActive;
            floodMeter = 1;
        }
    }

    /**
     * I actually have no idea what this does any more.
     * 
     * Me neither.
     * @deprecated 
     */
    private String unescape(String line) {
        StreamTokenizer parser = new StreamTokenizer(new StringReader(Util.unicodify(line)));
        try {
            parser.nextToken();
            if (parser.ttype == '"') {
                line = parser.sval;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }

    private void handleReply(String line) {
        if (line.contains("\"recaptchaRequired")) {
            callback.callback(this, "captcha", line.split("\"")[5]);
        } else if (line.contains("\"antinudeBanned\"")) {
            callback.callback(this, "ban", null);
        } else if (line.contains("\"typing\"")) {
            lastActive = System.currentTimeMillis();
            callback.callback(this, "typing", null);
        } else if (line.contains("\"question\"")) {
            // accepted = true;
            connected = true;
            lived = true;
            if (accepted) {
                birth = System.currentTimeMillis();
            }
            lastActive = System.currentTimeMillis();
            line = line.replaceAll(".*\"question\", *\"(.*?)\"\\](, *\\[\"[a-z][a-z]+.*|\\]$)", "\"$1\"");
            line = unescape(line);
            question = line;
            callback.callback(this, "connected", null);
            sleep(200);
            log("[" + nick + "][Question] " + line);
        } else if (line.contains("\"gotMessage\"")) {
            connected = true;
            lived = true;
            if (pulse) {
                sendPulse(false);
                pulse = false;
                callback.callback(this, "pulse success", null);
            }
            lastActive = System.currentTimeMillis();
            checkFlood();
            line = line.replaceAll(".*\"gotMessage\", *\"(.*)\"\\](, *\\[\"(stat|serv|ident).*|\\]$)", "\"$1\"");
            line = unescape(line);
            if (!accepted) {
                if (line.contains("I agree")) {
                    sendMessage("I agree");
                    sendMessage("although I'm a bot myself!");
                }
                if (line.toLowerCase().contains("toothpaste")
                        || line.contains("I agree") /*|| questionMode*/) {
                    accepted = true;
                    birth = System.currentTimeMillis();
                    callback.callback(this, "accepted", null);
                }
            }
            callback.callback(this, "message", line);
            if (accepted) {
                log("[" + nick + "] " + line);
            } else {
                logVerbose("[[UC] " + nick + "] " + line);
            }
            msgCount++;
        } else if (line.contains("\"strangerDisconnected\"")) {
            connected = false;
            callback.callback(this, "disconnected", null);
            log("[] " + nick + " is gone");
            msgCount = -1;
        } else if (line.contains("\"connected\"")) {
            if (!connected) {
                connected = true;
                lived = true;
                if (accepted) {
                    birth = System.currentTimeMillis();
                }
                if (pulse) {
                    sendPulse(false);
                    pulse = false;
                    log("Pulse joined with: " + getPulseWords());
                    callback.callback(this, "pulse success", null);
                }
                callback.callback(this, "connected", null);
                log("[] " + nick + " has entered the room");
            }
        }
    }

    private void generatePulse() {
        pulseName = names[(int) (Math.random() * names.length)];
        pulseWord = words[(int) (Math.random() * words.length)];
    }

}
